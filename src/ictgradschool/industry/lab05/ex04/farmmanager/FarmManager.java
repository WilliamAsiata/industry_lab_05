package ictgradschool.industry.lab05.ex04.farmmanager;

import ictgradschool.Keyboard;

public class FarmManager {
	public static void main(String[] args) {
		FarmManager fm = new FarmManager();
		fm.start();
	}

	private Farm farm;

	public FarmManager() {
		farm = new Farm();
	}

	/**
	 * Starts the farm manager going.
	 */
	public void start() {
		printWelcomeMessage();
		while (true) {
			// Get a command from the user and parse it
			String input = getCommand(); 
			if (input.equals("quit")) {
				break;
			}
			parseCommand(input);
			farm.tick();
		}
		printExitMessage();
	}

	private void printWelcomeMessage() {
		System.out.println("Welcome to Farm Manager 1964!");
	}

	private void printExitMessage() {
		System.out.println("Thanks for playing Farm Manager 1964!");
		System.out.println("Your farm finished with $" + farm.getMoney());

		int profit = farm.getMoney() - farm.getStartingMoney();
		if (profit > 0) {
			System.out.println("You made a profit of $" + profit);
		} else if (profit < 0) {
			System.out.println("You made a loss of $" + (-profit));
		} else {
			System.out.println("You finished where you started.");
		}
	}
	
	private String getCommand() {
		System.out.print(">> ");
		return Keyboard.readInput();
	}
	
	/**
	 * Parses a string into a command plus arguments and executes them.
	 *
	 * @param rawCmd The string to be parsed.
	 */
	private void parseCommand(String rawCmd) {
		String[] args = rawCmd.split(" ");

		// Empty commands can be ignored
		if (args.length == 0) {
			return;
		}

		String command = args[0];

		switch (command) {
			case "buy":
				buyAnimalCommand(args);
				break;
			case "stock":
				farm.printStock();
				break;
			case "money":
				System.out.println("$" + farm.getMoney());
				break;
			case "sell":
				System.out.println("You sold all your stock for $" + farm.sell());
				break;
			case "feed":
				feedAnimalCommand(args);
				break;
			case "harvest":
				farm.harvest();
				break;
			default:
				System.out.println("Unrecognised command");
		}
	}

    /**
     * Tells the farm to feed an animal type or all animals.
     *
     * @param args The arguments from the command.
     */
    private void feedAnimalCommand(String[] args) {
        // People should buy exactly one animal at a time
        if (args.length == 1) {
            farm.feedAll();
        } else if (args.length == 2) {
            farm.feed(args[1]);
        } else {
            System.out.println("Wrong number of arguments");
        }
    }

	/**
	 * Tells the farm to buy an animal.
	 *
	 * @param args The arguments from the command.
	 */
	private void buyAnimalCommand(String[] args) {
		// People should buy exactly one animal at a time

		if (args.length != 2) {
			System.out.println("Wrong number of arguments");
		}

		else {
            boolean success = farm.buyAnimal(args[1]);
			if (!success) {
				System.out.println("Could not buy a " + args[1] +
						". Either insufficient funds or the animal wasn't recognized.");
			}
		}
	}
}
