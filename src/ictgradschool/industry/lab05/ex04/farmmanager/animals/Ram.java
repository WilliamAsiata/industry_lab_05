package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Ram extends Sheep {

	/** All ram instances will have the same, shared, name: "ram" */
//	private static final String name = "ram";
//	public static final int maxValue = 300;

	public Ram() {
		value = 100;
		maxValue = 300;
	}

	@Override
	public int costToFeed() {
		return 15;
	}
}
