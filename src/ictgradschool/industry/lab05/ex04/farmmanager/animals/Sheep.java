package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Sheep extends Animal {

	/** All sheep instances will have the same, shared, name: "sheep" */
//	private static final String name = "sheep";

	public Sheep() {
		value = 90;
		maxValue = 200;
	}

	public int costToFeed() {
		return 10;
	}
}
