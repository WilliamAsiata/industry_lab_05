package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Cow extends Animal implements ProductionAnimals {

	/** All cow instances will have the same, shared, name: "cow" */
//	private static final String name = "cow";
	
	public Cow() {
		value = 1000;
		maxValue = 1500;
	}

	public void feed() {
		if (value < maxValue) {
			value += 100;
		}
	}

	public int costToFeed() {
		return 60;
	}

	@Override
	public boolean harvestable() {
		return time > 9;
	}

	@Override
	public int harvest() {
		if (harvestable()){
			time = 0;
			return 20;
		}
		return 0;
	}
}