package ictgradschool.industry.lab05.ex04.farmmanager.animals;

interface AnimalInterface {
	void feed();
	int costToFeed();
	String getName();
	int getValue();
}
