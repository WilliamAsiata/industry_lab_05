package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public class Chicken extends Animal implements ProductionAnimals {

	/** All chicken instances will have the same, shared, name: "chicken" */
//	private static final String name = "chicken";

	public Chicken() {
		value = 15;
		maxValue = 30;
	}

//	public String getName(){return name;}

	public int costToFeed() {
		return 5;
	}

	@Override
	public boolean harvestable() {
		return time > 2;
	}

	@Override
	public int harvest() {
		if (harvestable()){
			time = 0;
			return 3;
		}
		return 0;
	}
}