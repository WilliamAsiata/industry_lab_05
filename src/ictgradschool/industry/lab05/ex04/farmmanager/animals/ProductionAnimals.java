package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public interface ProductionAnimals {
	/**
	 * Is an animal able to be harvested?
	 */
	boolean harvestable();

	/**
	 * Harvests a crop from the animal.
	 *
	 * @return The money made from harvesting the animal. If the animal isn't harvestable return zero.
	 */
	int harvest();
}
