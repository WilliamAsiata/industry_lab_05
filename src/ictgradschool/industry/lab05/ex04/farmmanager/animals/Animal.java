package ictgradschool.industry.lab05.ex04.farmmanager.animals;

public abstract class Animal implements AnimalInterface {

//	protected String name;
	int value;
	public int time;
	int maxValue;

	//	public abstract int costToFeed();

	public void feed(){
		value += (maxValue - value)/2;
	}

	public String getName(){return getClass().getSimpleName();}

	public int getValue() {return value;}

	public String toString() {
		return getName() + " - $" + getValue();
	}

	public void tick() {time += 1;}
}
