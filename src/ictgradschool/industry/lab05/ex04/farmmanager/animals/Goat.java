package ictgradschool.industry.lab05.ex04.farmmanager.animals;

/**
 * Created by mtui016 on 27/03/2017.
 */
public class Goat extends Animal{

//    private static final String name = "Goat";

    private int value;

    public Goat(){value = 500;}

    @Override
    public void feed() {
        if (value < 1000) {
            value += 250;
        }
    }
    @Override
    public int costToFeed() {
        return 50;
    }

    @Override
    public int getValue() {
        return value;
    }
}
