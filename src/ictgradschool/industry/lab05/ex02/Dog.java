package ictgradschool.industry.lab05.ex02;

/**
 * Represents a dog.
 *
 * TODO Make this class implement the IAnimal interface, then implement all its methods.
 */
public class Dog implements IAnimal {

    private String name;

    Dog(String name){
        this.name = name;
    }

    @Override
    public String sayHello() {
        return myName() + " says woof woof.";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return name + " the " + this.getClass().getSimpleName();
    }

    @Override
    public int legCount() {
        return 4;
    }
}
