package ictgradschool.industry.lab05.ex02;

/**
 * Represents a Bird.
 *
 * Correctly implement these methods, as instructed in the lab handout.
 */
public class Bird implements IAnimal {

    private String name;

    Bird(String name){
        this.name = name;
    }

    @Override
    public String sayHello() {
        return myName() + " says tweet tweet.";
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        return name + " the "  + this.getClass().getSimpleName();
        //Alternatively
        // return name + " the " + this.getClass().getSimpleName();
    }

    @Override
    public int legCount() {
        return 2;
    }
}
