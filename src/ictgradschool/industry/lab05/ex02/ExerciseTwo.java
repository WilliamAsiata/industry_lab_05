package ictgradschool.industry.lab05.ex02;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird("Tweety");
        animals[1] = new Dog("Bruno");
        animals[2] = new Horse("Mr. Ed");

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // If the animal also implements IFamous, print out that corresponding info too.

        for (int i=0; i < list.length; i++){
            System.out.println(list[i].sayHello());
            if (list[i].isMammal()){
                System.out.println(list[i].myName() + " is a mammal.");
            } else{
                System.out.println(list[i].myName() + " is a non-mammal.");
            }
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");
            if (list[i] instanceof IFamous){
                IFamous animal = (IFamous) list[i];
                System.out.println("This is a famous name of my animal type: " + animal.famous());
            }
            System.out.println("-----------------------------------------------------------");
            // Find out if there is java markup to print a line of dashes without typing in a line of dashes.
        }
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
