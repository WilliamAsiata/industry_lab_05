package ictgradschool.industry.lab05.ex02;

/**
 * Represents a horse.
 *
 * TODO Make this implement IAnimal and IFamous, and provide appropriate implementations of those methods.
 */
public class Horse implements IAnimal, IFamous {

    private String name;

    public Horse(String name){
        this.name = name;
    }

    @Override
    public String sayHello() {
        return myName() + " says neigh.";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return name + " the "  + this.getClass().getSimpleName();
    }

    @Override
    public int legCount() {
        return 4;
    }

    @Override
    public String famous() {
        return "PharLap";
    }
}
